#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
#
#          FILE:  id3_romanizer.py
#
#        AUTHOR:  Jaeyoon Lee (lee@jaeyoon.org)
#       VERSION:  0.0.1
#       CREATED:  Fri May 23 20:20:50 PDT 2014
"""

import sys
import urllib2
from mutagen.easyid3 import EasyID3
from urllib import quote
from translate import Translator

class Trans(Translator):
    def _get_json5_from_google(self, source):
        escaped_source = quote(source.encode('utf8'))
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.132 Safari/537.36 OPR/21.0.1432.67'}
        req = urllib2.Request(
            url="http://translate.google.com/translate_a/t?client=t&ie=UTF-8&oe=UTF-8"
                +"&sl=%s&tl=%s&text=%s" % (self.from_lang, self.to_lang, escaped_source)
                , headers = headers)
        r = urllib2.urlopen(req)
        return r.read().decode('utf-8')


def get_translation(source):
    try:
        data = source.encode('latin-1').decode('euc-kr')
    except:
        data = source

    romanizer= Trans(to_lang="en", from_lang="ko")

    return romanizer.translate(data)


def main():
    from glob import glob
    from optparse import OptionParser

    parser = OptionParser(usage="%prog [-p path]", version="%prog 0.0.1")
    parser.add_option("-p", "--path", dest="path", action="store", type="string",
                      help="A path for music files")
    (options, args) = parser.parse_args()

    if not options.path:
        print "File path must be specified"
        sys.exit(1)

    for filename in glob(options.path):
        print "# %s" % filename
        id3 = EasyID3(filename)
        for item in id3.items():
            (key, value) = item
            old_value = id3[key]
            id3[key] = get_translation(value[0])
            print "- %s (%s) -> %s" % (old_value, value[0], id3[key])
            id3.save()


if __name__=='__main__':
    main()
