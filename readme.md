# ID3 Tag Romanizer #
Author: Jaeyoon Lee
Date: Sun May 25 00:50:03 PDT 2014

### Description ###
An ID3 tag is a data container within an MP3 audio file stored in a prescribed format. This data commonly contains the Artist name, Song title, Year and Genre of the current audio file. 

This tool romanizes/translates and replaces the data in a ID3 tag from Korean to English. The only use case of this tool is the car audio systems in US which do not support multi byte character display, and it makes very hard to navigate a playlist to choose a song I want to listen at the moment. Indeed, hacking into each car audio system firmware could be a way-to-go, but this tool is enough for me with little bit of awkwardness but lazy safe.

### Dependencies ###
* [translator](https://github.com/terryyin/google-translate-python)
* [mutagen](https://code.google.com/p/mutagen/)

### How to setup ###
```
; Clone this repository
git clone git@bitbucket.org:jcrew/id3tag.romanizer.git

; Install the dependencies
pip install translator mutagen
```

### Usage ###
```
Usage: id_romanizer.py [-p path]

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -p PATH, --path=PATH  A path contatins mp3 files
```

### Sample Output ###
```
farenheit:id3.romanizer jcrew$ ./id3_romanizer.py -p "sample/*"
# sample/1-01 Deuxforever (Intro).mp3
- [u'1996'] (1996) -> [u'1996']
- [u'\ub4c0\uc2a4(Deux)'] (듀스(Deux)) -> [u'Deux (Deux)']
- [u'1/13'] (1/13) -> [u'1/13']
- [u'Deux Forever'] (Deux Forever) -> [u'Deux Forever']
- [u'R&B/Hip-Hop'] (R&B/Hip-Hop) -> [u'R & B / Hip-Hop']
- [u'1/2'] (1/2) -> [u'1/2']
- [u'\ub4c0\uc2a4(Deux)'] (듀스(Deux)) -> [u'Deux (Deux)']
- [u'Deuxforever (Intro)'] (Deuxforever (Intro)) -> [u'Deuxforever (Intro)']
# sample/1-02 나를 돌아봐.mp3
- [u'1996'] (1996) -> [u'1996']
- [u'\ub4c0\uc2a4(Deux)'] (듀스(Deux)) -> [u'Deux (Deux)']
- [u'2/13'] (2/13) -> [u'2/13']
- [u'Deux Forever'] (Deux Forever) -> [u'Deux Forever']
- [u'R&B/Hip-Hop'] (R&B/Hip-Hop) -> [u'R & B / Hip-Hop']
- [u'1/2'] (1/2) -> [u'1/2']
- [u'\ub4c0\uc2a4(Deux)'] (듀스(Deux)) -> [u'Deux (Deux)']
- [u'\ub098\ub97c \ub3cc\uc544\ubd10'] (나를 돌아봐) -> [u'Turn around me']
# sample/1-03 알고 있었어.mp3
- [u'1996'] (1996) -> [u'1996']
- [u'\ub4c0\uc2a4(Deux)'] (듀스(Deux)) -> [u'Deux (Deux)']
- [u'3/13'] (3/13) -> [u'3/13']
- [u'Deux Forever'] (Deux Forever) -> [u'Deux Forever']
- [u'R&B/Hip-Hop'] (R&B/Hip-Hop) -> [u'R & B / Hip-Hop']
- [u'1/2'] (1/2) -> [u'1/2']
- [u'\ub4c0\uc2a4(Deux)'] (듀스(Deux)) -> [u'Deux (Deux)']
- [u'\uc54c\uace0 \uc788\uc5c8\uc5b4'] (알고 있었어) -> [u'Did you know']
# sample/1-04 그대. 지금. 다시.mp3
- [u'1996'] (1996) -> [u'1996']
- [u'\ub4c0\uc2a4(Deux)'] (듀스(Deux)) -> [u'Deux (Deux)']
- [u'4/13'] (4/13) -> [u'4/13']
- [u'Deux Forever'] (Deux Forever) -> [u'Deux Forever']
- [u'R&B/Hip-Hop'] (R&B/Hip-Hop) -> [u'R & B / Hip-Hop']
- [u'1/2'] (1/2) -> [u'1/2']
- [u'\ub4c0\uc2a4(Deux)'] (듀스(Deux)) -> [u'Deux (Deux)']
- [u'\uadf8\ub300. \uc9c0\uae08. \ub2e4\uc2dc'] (그대. 지금. 다시) -> [u'You . Right now. Again']
```